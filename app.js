const _ = require('lodash')
const yargs = require('yargs')

const notes = require('./notes')

const title = {
    describe: 'Title of note',
    demand: true,
    alias: 't'
}

const body = {
    describe: 'Body of note',
    demand: true,
    alias: 'b'
}

let argv = yargs
    .command('add', 'Add a new note',{title,body})
    .command('list', 'List notes')
    .command('read', 'Read a note', {title})
    .command('remove', 'Remove the note', {title})
    .help()
    .argv
let command = argv._[0]

if (command === 'add') {
    let note = notes.addNote(argv.title, argv.body)

    if (note) {
        console.log('Note created')
        console.log('-----')
        console.log(`Title:${note.title}`)
        console.log(`Body: ${note.body}`)
    } else {
        console.log('Note title taken')
    }

} else if ( command === 'list') {
    let list = notes.getAll()
    console.log(list)
} else if (command === 'read') {
    let readNote = notes.getNote(argv.title)
    if (readNote) {
        console.log('Note read')
        console.log('-----')
        console.log(`Title:${readNote[0].title}`)
        console.log(`Body: ${readNote[0].body}`)
    } else {
        console.log("Note doesn't exist")
    }
} else if (command === 'remove'){
    let noteRemoved = notes.removeNote(argv.title)
    let message = noteRemoved ? 'Note was removed': 'Note not found'
    console.log(message)
}else {
    console.log('Command is not recognized')
}

console.log('Command:', command)
